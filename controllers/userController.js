const { validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const UserModel = require('../models/userModel');
const dotenv = require('dotenv');

dotenv.config();

const userModel = new UserModel();
class UserController {

    async register(req, res) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const { name, email, password } = req.body;

        try {
            const existingUser = await userModel.findUserByEmail(email);
            if (existingUser) {
                return res.status(400).json({ message: 'Email is already in use' });
            }

            const hashedPassword = await bcrypt.hash(password, 10);
            const data = await userModel.createUser(name, email, hashedPassword);



            res.status(201).json({success: true, result: data , message: 'User registered successfully' });
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'Server Error' });
        }
    }

    async login(req, res) {
        try {
            const { email, password } = req.body;

            const user = await userModel.findUserByEmail(email);
            if (!user) {
                return res.status(400).json({ message: 'User not found' });
            }

            const passwordMatch = await bcrypt.compareSync(password, user.password);

            if (!passwordMatch) {
                return res.status(400).json({ message: 'Invalid password'});
            }

            const token = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + 60 * 60 * 12,
                id: user.id,
            }, process.env.JWT_SECRET);

            res.status(201).json({
                success: true, result: {
                    token,
                }, message: "Successfully logged in",
            });
        } catch (err) {
            res.status(500).json({success: false, result: null, message: err.message});
        }
    }

    async getUserInfo(req, res) {
        try {
            if (!req.user) {
                return res.status(401).json({ message: 'User not found' });
            }
            res.status(201).json({success: true, result: req.user , message: "Successfully get",});
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'Server Error' });
        }
    }
}

module.exports = UserController;