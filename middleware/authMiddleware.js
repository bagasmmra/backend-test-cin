const jwt = require('jsonwebtoken');
const UserModel = require('../models/userModel');
const dotenv = require('dotenv');

dotenv.config();
const authenticateJWT = async (req, res, next) => {
    const token = req.header('Authorization');

    if (!token) {
        return res.status(401).json({ message: 'Authorization token is missing' });
    }

    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const userModel = new UserModel(); // Create a new UserModel instance
        req.user = await userModel.findUserById(decoded.id);
        next();
    } catch (err) {
        console.error(err);
        res.status(401).json({ message: 'Invalid token' });
    }
};

module.exports = authenticateJWT;