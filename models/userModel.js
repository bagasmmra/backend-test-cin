const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

class UserModel {
    async createUser(name, email, password) {
        try {
            return await prisma.user.create({
                data: {
                    name,
                    email,
                    password,
                },
                select: {
                    name: true,
                    email: true,
                },
            });
        } catch (error) {
            throw error;
        }
    }

    async findUserByEmail(email) {
        try {
            const user = await prisma.user.findUnique({
                where: {
                    email,
                },
            });
            return user || null; // Return the user if found, or null if not found
        } catch (error) {
            throw error;
        }
    }

    async findUserById(userId) {
        try {
            const user = await prisma.user.findUnique({
                where: {
                    id: userId,
                },
                select: {
                    name: true,
                    email: true,
                },
            });
            return user || null; // Return the user if found, or null if not found
        } catch (error) {
            throw error;
        }
    }
}

module.exports = UserModel;