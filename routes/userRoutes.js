const express = require('express');
const { check } = require('express-validator');
const UserController = require('../controllers/userController');
const authenticateJWT = require('../middleware/authMiddleware');

const router = express.Router();
const userController = new UserController();

router.post(
    '/register',
    [
        check('name').notEmpty().withMessage('Name is required'),
        check('email').isEmail().withMessage('Invalid email address'),
        check('password')
            .isLength({ min: 6 })
            .withMessage('Password must be at least 6 characters long'),
    ],
    userController.register
);

router.post('/login', userController.login);

router.get('/me', authenticateJWT, userController.getUserInfo);

module.exports = router;